<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## How to use

- git clone git@gitlab.com:humamalamin/otoklix-laravel.git
- cd otoklix-laravel
- run composer install
- cp .env.example to .env
- change database driver to sqlite and DB_DATABASE path file database.sqlite
- run php artisan key:generate
- run php artisan migrate --seed
- run php artisan serve for test in local

## How to run unit testing
 
- cd otoklix-laravel
- run .\vendor\bin\phpunit --debug
