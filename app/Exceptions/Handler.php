<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use App\Http\Resources\ErrorResource;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\QueryException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        if ($request->wantsJson()) {
            $this->renderable(function (NotFoundHttpException $e, $request) {
                return new ErrorResource($e, 404, 'ERR4004');
            });

            $this->renderable(function (ValidationException $e, $request) {
                return new ErrorResource($e->errors(), 422, 'ERR4022');
            });

            $this->renderable(function (\Exception $e) {
                return new ErrorResource($e);
            });
        }

        return parent::render($request, $e);
    }
}
