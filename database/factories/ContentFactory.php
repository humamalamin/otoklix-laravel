<?php

namespace Database\Factories;

use App\Models\Content;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContentFactory extends Factory
{
    protected $model = Content::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word,
            'content' => $this->faker->word,
            'published_at' => Carbon::now()
        ];
    }
}
