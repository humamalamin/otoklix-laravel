<?php

namespace Tests\Feature;

use App\Models\Content;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class ContentTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testListContent()
    {
        $response = $this->getJson(route('posts.index'));

        $response->assertStatus(200)
            ->assertJson([
                'data' => []
            ]);
    }

    public function testShowContent()
    {
        $post = Content::factory()->create();
        $response = $this->getJson(route('posts.show', ['post' => $post->id]));

        $response->assertStatus(200)
            ->assertJson(function (AssertableJson $json) use ($post) {
                $json->has('data', function ($json) use ($post) {
                        $json->where('id', $post->id)
                            ->where('title', $post->title)
                            ->where('content', $post->content)
                            ->etc();
                    })
                    ->etc();
            });
    }

    public function testShowNotFoundContent()
    {
        $response = $this->getJson(route('posts.show', ['post' => 'as']));

        $response->assertJson([
            'errors' => [
                "No query results for model [App\\Models\\Content] as"
            ]
        ]);
    }

    public function testStore()
    {
        $param = [
            'title' => 'test',
            'content' => 'testas'
        ];

        $response = $this->post(route('posts.store'), $param);
        $response->assertStatus(201)
                 ->assertJson(['success' => true])
                 ->assertJson(function (AssertableJson $json) use ($param) {
                     $json->has('data', function ($json) use ($param) {
                              $json->where('title', $param['title'])
                                   ->where('content', $param['content'])
                                   ->etc();
                          })
                          ->etc();
                 });
        $this->assertDatabaseHas('contents', [
            'title' => $param['title'],
            'content' => $param['content'],
        ]);
    }

    public function testStoreFailRequiredFieldTitle()
    {
        $param = [
            'title' => '',
            'content' => 'testas'
        ];

        $response = $this->post(route('posts.store'), $param);
        $response->assertStatus(422)
                 ->assertJson(['success' => false])
                 ->assertJson([
                     'errors' => [
                        'title' => [
                            "The title field is required."
                        ]
                     ]
                 ]);
    }

    public function testEdit()
    {
        $post = Content::factory()->create();
        $response = $this->put(
            route('posts.update', [$post->id]),
            [
                'title' => 'TEST',

                'content' => 'TEST ASA'
            ]
        );

        $response->assertStatus(200)
                 ->assertJson(['success' => true])
                 ->assertJson(function (AssertableJson $json) use ($post) {
                     $json->has('data', function ($json) use ($post) {
                              $json->where('id', $post->id)
                                   ->where('title', 'TEST')
                                   ->where('content', 'TEST ASA')
                                   ->etc();
                          })
                          ->etc();
                 });
        $this->assertDatabaseHas('contents', [
            'id' => $post->id,
            'title' => 'TEST',
            'content' => 'TEST ASA'
        ]);
    }

    public function testEditFailure()
    {
        $post = Content::factory()->create();
        $response = $this->put(
            route('posts.update', [$post->id]),
            [
                'title' => 'TEST',
            ]
        );

        $response->assertStatus(422)
                 ->assertJson(['success' => false])
                 ->assertJson([
                    'errors' => [
                       'content' => [
                           "The content field is required."
                       ]
                    ]
                ]);
    }

    public function testDelete()
    {
        $post = Content::factory()->create();
        $response = $this->deleteJson(route('posts.update', [$post->id]));

        $response->assertStatus(200)
                 ->assertJson(['success' => true])
                 ->assertJson(['data' => []]);
        $this->assertDatabaseMissing('contents', [
            'id' => $post->id,
        ]);
    }
}
