<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->seedDatabase();

        $this->withHeaders([
            'accept' => 'application/json',
        ]);
    }

    protected function seedDatabase(): void
    {
        $this->seed('DatabaseSeeder');
    }
}
